#include "Tester.h"
#include "SolveSquare.h"
#include <cstdio>
using namespace SolveSquare;

bool Tester( char fileName[] )
{
	FILE* tests = fopen( fileName, "r" );
	if( ferror( tests ) != 0 ) return false;
 
	int tests_Count = 0;
	fscanf( tests, "%i", &tests_Count );
	
	bool Tester_Result = true; 

	for( int i = 1; i <= tests_Count; i++ )
	{
		printf( "Test #%i\n", i );
		
		bool Test_Result = true;

		float a = 0, b = 0, c = 0;
		fscanf( tests, "%f%f%f", &a, &b, &c );
		int nRoots_Waited = 0;
		fscanf( tests, "%i", &nRoots_Waited );
		double root_1_Waited = 0, root_2_Waited = 0;

		switch( nRoots_Waited )
		{
			case 0: { break; }
			case 1: { fscanf( tests, "%lf", &root_1_Waited ); break; }
			case 2: { fscanf( tests, "%lf%lf", &root_1_Waited, &root_2_Waited ); break; }
			case SS_INF_ROOTS: { break; }
		}
				  
		double root_1 = 0, root_2 = 0;
		int nRoots = solveSquare( a, b, c, &root_1, &root_2 );
		
		if( nRoots != nRoots_Waited ) 
		{
			printf( "Test failed: a = %f, b = %f, c = %f; nRoots( %i ) != nRoots_Waited( %i )\n", a, b, c, nRoots, nRoots_Waited );
			Test_Result = false;
		}			

		switch( nRoots )
		{
				 
			case 1: 
			{ 
				if( root_1 != root_1_Waited )
				{
					printf( "Test failed: a = %f, b = %f, c = %f; root_1( %lf ) != root_1_Waited( %lf )\n", a, b, c, root_1, root_1_Waited );
					Test_Result = false;  
				}

				break;
			}
			case 2: 
			{ 
				if( root_1 != root_1_Waited )
				{
					printf( "Test failed: a = %f, b = %f, c = %f; root_1( %lf ) != root_1_Waited( %lf )\n", a, b, c, root_1, root_1_Waited );
					Test_Result = false;  
				}

				if( root_2 != root_2_Waited )
				{
					printf( "Test failed: a = %f, b = %f, c = %f; root_2( %lf ) != root_2_Waited( %lf )\n", a, b, c, root_2, root_2_Waited );
					Test_Result = false;  
				}

				break; 
			}
		}
		
		if( Test_Result == true ) printf( "Test OK\n" );
		Tester_Result = Tester_Result && Test_Result;		
	}	
	
	return Tester_Result;
			
}
