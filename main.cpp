#include "SolveSquare.h"
#include "Tester.h"
#include <cstdio>
#include <cstring>
#include <cmath>

using namespace SolveSquare;

int main( int argc, char* argv[] )
{

	if( argc > 1 )
	{
		if( strcmp( argv[1],  "-t" ) == 0  && argc == 3 )
		{
			bool Tester_Result = Tester( argv[2] );
			if ( Tester_Result == true ) printf( "All tests completed\n" );
			else printf( "Tests failed\n" );

			return 0;
		}
		else
		{
			printf( "Unknown commands\n" );
			return 1;
		}
	}

	printf( "# SolveSquare\n# v 1.2 imcub 2017\n" );
	
	printf( "Input coefficents of your square equation ( ax^2 + bx + c = 0 ): " );
	
	double a = NAN, b = NAN, c = NAN;
	scanf( "%lf%lf%lf", &a, &b, &c );
	
	double root_1 = 0, root_2 = 0;
	int nRoots = solveSquare( a, b, c, &root_1, &root_2 );
	
	printf( "Roots:\n" );
	switch( nRoots )
	{
		case 0:
		{	
			printf( "No roots in your equation\n" );
			break;
		}
		case 1:
		{
			printf( "Root = %.5lf\n", root_1 );
			break;
		}
		case 2:
		{
			printf( "Root_1 = %.5lf\n"
			        "Root_2 = %.5lf\n", root_1, root_2 );
			break;
		}
		case SS_INF_ROOTS:
		{	
			printf( "Your equation has infinite roots\n" );
			break;
		}
		default:
		{	
			printf( "# Unexpected error\n" );
			return 1;
		}
		
	}
	return 0;
}

