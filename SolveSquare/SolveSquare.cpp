#include "SolveSquare.h"
#include <cmath>
#include <cassert>
#include <cstdlib>

namespace SolveSquare
{

	int solveSquare( double a, double b, double c, double* root_1, double* root_2 )
	{
		assert( std::isfinite(a) );
		assert( std::isfinite(b) );
		assert( std::isfinite(c) );
		assert( root_1 != nullptr );
		assert( root_2 != nullptr );
		assert( root_1 != root_2 );
		
		if( fabs( a ) < EPS ) return solveLinear( b, c, root_1 );
		else
		{
			double D = b*b - 4*a*c;
				
			if( D < 0 - EPS )
			{
				return 0;
			}
			  
			if( fabs( D ) < EPS )
			{
				*root_1 = -1*b / ( 2*a ); 
				return 1;
			}
			
			if( D > 0 + EPS )
			{
				double sqrt_D = sqrt( D );
				*root_1 = ( -1*b - sqrt_D ) / ( 2*a ); 
				*root_2 = ( -1*b + sqrt_D ) / ( 2*a );
				return 2;
			}
		}
        return -1;
	}
	
	int solveSquare( double a, double b, double c )
	{
		assert( std::isfinite(a) );
		assert( std::isfinite(b) );
		assert( std::isfinite(c) );
		
		if( a == 0 ) return solveLinear( b, c );
		else
		{
			double D = b*b - 4*a*c;
				
			if( D < 0 ) return 0;
			  
			if( D == 0 ) return 1;
			
			if( D > 0 ) return 2;
		}
        return -1;
	}

	int solveLinear( double m, double n )
	{
		assert( std::isfinite( m ) );
		assert( std::isfinite( n ) );

		if( m == 0 )
		{
			if ( n == 0 ) return SS_INF_ROOTS;
			else return 0;
		}
		else return 1;
	}

	int solveLinear( double m, double n, double* root )
	{
		assert( std::isfinite( m ) );
		assert( std::isfinite( n ) );
		assert( root != nullptr );

		if( m == 0 )
		{
			if ( n == 0 ) return SS_INF_ROOTS;
			else return 0;
		}	
		else 
		{
			*root = n / ( -1*m );
			return 1;
		}
	}
}
