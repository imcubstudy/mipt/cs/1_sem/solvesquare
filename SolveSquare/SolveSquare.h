#include <cfloat>

namespace SolveSquare
{
	const int SS_INF_ROOTS = 3;
    const double EPS = DBL_EPSILON*100;

	int solveSquare( double a, double b, double c, double* root_1, double* root_2 );
	int solveSquare( double a, double b, double c );
	int solveLinear( double m, double n );
	int solveLinear( double m, double n, double* root );
}
